/**
 * App Entry File
 * Vuely: A Powerfull Material Design Admin Template
 * Copyright 2018. All Rights Reserved
 * Created By: The Iron Network, LLC
 * Made with Love
 */
import Vue from 'vue'
import Vuetify from 'vuetify'
import * as VueGoogleMaps from 'vue2-google-maps'
import {
	Vue2Dragula
} from 'vue2-dragula'
import VueQuillEditor from 'vue-quill-editor'
import wysiwyg from 'vue-wysiwyg'
import VueBreadcrumbs from 'vue2-breadcrumbs'
import VueResource from 'vue-resource'
import VueNotifications from 'vue-notifications'
import miniToastr from 'mini-toastr'
import AmCharts from 'amcharts3'
import AmSerial from 'amcharts3/amcharts/serial'
import AmAngularGauge from 'amcharts3/amcharts/gauge'
import VModal from 'vue-js-modal'
import Nprogress from 'nprogress'
import VueI18n from 'vue-i18n'
import VueTour from 'vue-tour'
import fullscreen from 'vue-fullscreen'
import InstantSearch from 'vue-instantsearch'
import $ from 'jquery';




// global components
import GlobalComponents from './globalComponents'

// app.vue
import App from './App'

// router
import router from './router'

// themes
import primaryTheme from './themes/primaryTheme';

// store
import {
	store
} from './store/store';

// firebase
import firebase from 'firebase';
import './firebase'
import Firebase from 'firebase';
import {
	database
} from "./firebase/";


// include script file
import './lib/VuelyScript'

// include all css files
import './lib/VuelyCss'

// messages
import messages from './lang';
/*
// navigation guards before each
router.beforeEach((to, from, next) => {
	Nprogress.start()
	if (to.matched.some(record => record.meta.requiresAuth)) {
		// this route requires auth, check if logged in
		// if not, redirect to login page.
		if (store.getters.getUser === null) {
			next({
				path: '/session/login',
				query: { redirect: to.fullPath }
			})
		} else {
			next()
		}
	} else {
		next() // make sure to always call next()!
	}
})*/
/*
// navigation guards before each
router.beforeEach((to, from, next) => {
	document.title = to.meta.title;
	console.log("entrando beforeeach ",store.getters.logged)
	console.log("document.title  ",document.title )
	Nprogress.start()
	
	if (to.matched.some(record => record.meta.requiresAuth)) {
		// this route requires auth, check if logged in
		// if not, redirect to login page.
					
		console.log("entra requiresAuth" )
		if (store.getters.logged === false) { 
			
			console.log("entra netxt0",store.getters.logged )
			next({
				path: '/session/login',
				//query: { redirect: to.fullPath }
			})
			
		} else {
			console.log("entra netxt1")
			next()
		}
	} else {
		console.log("entra netxt2")
		next() // make sure to always call next()!
	}

})

*/
// navigation guard after each
router.afterEach((to, from) => {
	Nprogress.done()
	setTimeout(() => {
		$('.v-content__wrap').scrollTop(0, 0)
		$('.app-boxed-layout .app-content').scrollTop(0, 0)
		$('.app-mini-layout .app-content').scrollTop(0, 0)
	}, 200);
})

function toast({
	title,
	message,
	type,
	timeout,
	cb
}) {
	return miniToastr[type](message, title, timeout, cb)
}

const options = {
	success: toast,
	error: toast,
	info: toast,
	warn: toast
}

const toastTypes = {
	success: 'success',
	error: 'error',
	info: 'info',
	warn: 'warn'
}

miniToastr.init({
	types: toastTypes
})

// plugins
Vue.use(Vuetify, {
	theme: store.getters.selectedTheme.theme
});
Vue.use(InstantSearch);
Vue.use(VueI18n)
Vue.use(AmCharts)
Vue.use(VModal)
Vue.use(AmSerial)
Vue.use(VueTour)
Vue.use(AmAngularGauge)
Vue.use(Vue2Dragula)
Vue.use(VueQuillEditor)
Vue.use(VueResource)
Vue.use(wysiwyg, {})
Vue.use(VueBreadcrumbs)
Vue.use(VueNotifications, options)
Vue.use(fullscreen);
Vue.use(GlobalComponents);
Vue.use(VueGoogleMaps, {
	load: {
		key: 'AIzaSyBtdO5k6CRntAMJCF-H5uZjTCoSGX95cdk' // Add your here your google map api key
	}
})

// Create VueI18n instance with options
const i18n = new VueI18n({
	locale: store.getters.selectedLocale.locale, // set locale
	messages, // set locale messages
})

Firebase.auth().onAuthStateChanged(function (user) {
/* eslint-disable no-new */
let app;
if (!app) {
app =new Vue({
	el: '#app',
	store,
	
	i18n,
	router,
	template: '<App/>',
	components: {
		App
	},
	created() {


		
/*

		firebase.auth().onAuthStateChanged((user) => {
			console.log("onAuthStateChanged",user)
			if (user) {
				database.collection('users').doc(user.uid).onSnapshot(snapshot => {
					//	console.log("---sd",user)
				//	console.log("store.state.auth.logged: -->", user)
					store.commit('setUser', user);
					if (snapshot.exists) {
						store.commit('setRole', snapshot.data().role);
						if (snapshot.data().role === 'admin') {
							store.dispatch('createDashboardIfNotExists', user);
						}
					}
					console.log("entra index.js setload tree 1")
					
					store.commit('setLoaded', true);

				})
			} else {
			
				console.log("entra index.js setload tree 2")
			//	store.commit('setLoaded', true);

			}
		})

		

	}*/
 }


});
 }
});