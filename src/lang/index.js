import en from './en';
import es from './es';
import fr from './fr';
import he from './he';
import ru from './ru';
import ar from './ar';

export default {
    en: {
        message: en
    },
    es: {
        message: es
    },
    fr: {
        message: fr
    },
    he: {
        message: he
    },
    ru: {
        message: ru
    },
    ar: {
        message: ar
    }
}