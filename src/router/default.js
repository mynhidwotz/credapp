import Full from 'Container/Full'

// dashboard components
const Ecommerce = () => import('Views/dashboard/Ecommerce');



const regNinos = () => import('Views/dashboard/regNinos');

const listaNinos = () => import('Views/dashboard/listaNinos');

const visitaMedica = () => import('Views/dashboard/visitaMedica'); 
const statNinos = () => import('Views/dashboard/statNinos');  
const histoVisitas = () => import('Views/dashboard/histoVisitas');
const statVisitas = () => import('Views/dashboard/statVisitas');
const statMediciones = () => import('Views/dashboard/statMediciones');

const configUsuario = () => import('Views/dashboard/configUsuario');  
const configCustom = () => import('Views/dashboard/configCustom');  

const usoAyuda = () => import('Views/dashboard/usoAyuda');
const problemaAyuda = () => import('Views/dashboard/problemaAyuda');

export default {
   path: '/',
   component: Full,
   redirect: '/default/dashboard/ecommerce',

   meta: { requiresAuth: true,  role: 'admin' },
   children: [
    {
         path: '/default/dashboard/ecommerce',
         component: Ecommerce,
         meta: {
            requiresAuth: true,
            role: 'admin',
            title: 'message.ecommerce',
            breadcrumb: 'Dashboard / Ecommerce'
         }
      },
      {
        path: '/default/dashboard/regNinos',
        component: regNinos,
        meta: {
           requiresAuth: true,
           role: 'admin',
           title: 'message.regNinos',
           breadcrumb: 'Dashboard / Niños / Registro de Niños'
        }
     },

     {
        path: '/default/dashboard/listaNinos',
        component: listaNinos,
        meta: {
           requiresAuth: true,
           title: 'message.listaNinos',
           breadcrumb: 'Dashboard / Niños / Listado'
        }
     }, 
     {
      path: '/default/dashboard/statNinos',
      component: statNinos,
      meta: {
         requiresAuth: true,
         title: 'message.statNinos',
         breadcrumb: 'Dashboard / Niños / Estadística'
      }
   },
     
     {
        path: '/default/dashboard/visitaMedica',
        component: visitaMedica,
        meta: {
           requiresAuth: true,
           title: 'message.visitaMedica',
           breadcrumb: 'Dashboard / Visita / Examen '
        }
     },  
     {
      path: '/default/dashboard/histoVisitas',
      component: histoVisitas,
      meta: {
         requiresAuth: true,
         title: 'message.histoVisitas',
         breadcrumb: 'Dashboard / Visita / Historial ' 
      }
   },  

   {
    path: '/default/dashboard/statVisitas',
    component: statVisitas,
    meta: {
       requiresAuth: true,
       title: 'message.statVisitas',
       breadcrumb: 'Dashboard / Visita / Estadística ' 
    }
 },
 
 {
  path: '/default/dashboard/statMediciones',
  component: statMediciones,
  meta: {
     requiresAuth: true,
     title: 'message.statMediciones',
     breadcrumb: 'Dashboard / Visita / Estadística de Mediciones ' 
  }
},
{
  path: '/default/dashboard/configUsuario',
  component: configUsuario,
  meta: {
     requiresAuth: true,
     title: 'message.userProfile',
     breadcrumb: 'Dashboard / Estadística de Mediciones ' 
  }
}, 
{
  path: '/default/dashboard/configCustom',
  component: configCustom,
  meta: {
     requiresAuth: true,
     title: 'message.configCustom',
     breadcrumb: 'Dashboard / Configuración / Personalización ' 
  }
},  
{
  path: '/default/dashboard/usoAyuda',
  component: usoAyuda,
  meta: {
     requiresAuth: true,
     title: 'message.usoAyuda',
     breadcrumb: 'Dashboard / Ayuda / Uso' 
  }
},
{
  path: '/default/dashboard/problemaAyuda',
  component: problemaAyuda,
  meta: {
     requiresAuth: true,
     title: 'message.problemaAyuda',
     breadcrumb: 'Dashboard /  Ayuda / Reporte de problemas ' 
  }
}

   ]

}