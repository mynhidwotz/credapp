import Vue from 'vue'
import Router from 'vue-router'
import Full from 'Container/Full'
import Firebase from 'firebase';
import firebase from 'firebase';
//routes
import defaultRoutes from './default';
import horizontalRoutes from './horizontal';
import boxedRoutes from './boxed';
import mini from './mini';




// session components
const Home = () => import('Views/Home');
const SignUpOne = () => import('Views/session/SignUpOne');
const LoginOne = () => import('Views/session/LoginOne');
//const Auth0CallBack = () => import('Components/Auth0Callback/Auth0Callback');
const Ecommerce = () => import('Views/dashboard/Ecommerce');


Vue.use(Router)
// store
import { store } from '../store/store';
/*
const beforeEnter = (to, from, next) => {
	console.log("entrando beforeEnter ",store.getters.getUser )
	if (store.state.auth.getUser) {
		
		console.log("--->beforeEnter")
	  next({path: '/'});

	} else {
	next();
	  console.log("else--->beforeEnter",store.getters.getUser)
	  
	}
  };
*/
/*
export default new Router({
	mode: 'history',
	routes: [
		{
			path: '/session/sign-up',
			component: SignUpOne,
			meta: {
				requiresAuth: false,
				title: 'message.signUp',
				//breadcrumb: 'Session / Sign Up'
			}
		},
		{
			path: '/session/login',
			component: LoginOne,
			meta: {
				requiresAuth: false,
				title: 'message.login',
				//breadcrumb: 'Session / Login'
				
			},
		//	beforeEnter: (to, from, next) => beforeEnter(to, from, next)
		},
		defaultRoutes,
		horizontalRoutes,
		boxedRoutes,

		
		mini,

	]



	
})
*/
const beforeEnter = (to, from, next) => {
	
	if (firebase.auth().currentUser) {
 console.log("beforeEnter /")
		next({path: '/'});
	} else {
		console.log("beforeEnter next")
		next();
	
	}
	};
	

const router = new Router({
	mode: 'history',
	routes: [
		{
			path: '/session/sign-up',
			component: SignUpOne,
			meta: {
				requiresAuth: false,
				title: 'message.signUp',
				requiresGuest: true
								//breadcrumb: 'Session / Sign Up'
			}
		},
		{
			path: '/login',
			component: LoginOne,
			meta: {
				requiresAuth: false,
				title: 'message.login',
				requiresGuest: true
				//breadcrumb: 'Session / Login'
				
			},
	//		beforeEnter: (to, from, next) => beforeEnter(to, from, next)
		},
		{
			path: '/dashboard',
			name: 'dashboard',
		component: Full,
		meta: {			requiresAuth: true 			},
		
			children: [ 
			{
				path: '/default/dashboard/ecommerce',
				component: Ecommerce,
				meta: {			requiresAuth: true 			},
			
			}
		]
		},

		{
			path: '',
		
			name: 'home',
    component: Home,
		//	redirect: '/default/dashboard/ecommerce',
		 /*
			meta: { requiresAuth: true,  role: 'admin' },
			children: [
			 {
				  path: '/default/dashboard/ecommerce',
				  component: Ecommerce,
				  meta: {
					requiresAuth: true,
					role: 'admin',
					 title: 'message.ecommerce',
					
				  }
			   },

			]*/

			
		}

		//defaultRoutes,
		//horizontalRoutes,
	//	boxedRoutes,

		
	//	mini,

	]

});

router.beforeEach((to, from, next) => {

  const currentUser = Firebase.auth().currentUser;
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth);

  if (requiresAuth && !currentUser) {
    next('/login');
  } else if (requiresAuth && currentUser) {
    next();
  } else {
    next();
  }

});

/*
router.beforeEach((to, from, next) => {
  document.title = to.meta.title;
  if (to.meta.requiresAuth && !store.state.auth.logged && store.state.loaded) {
    next({path: '/session/login'});
  } else {
    if (to.meta.role) {
      if (store.state.loaded && (to.meta.role !== store.state.auth.role)) {
        next({path: '/'});
        return;
      }
    }
    next();
  }
});
*/
/*
// Nav Guard
router.beforeEach((to, from, next) => {
  // Check for requiresAuth guard
  if (to.matched.some(record => record.meta.requiresAuth)) {
    // Check if NO logged user
    if (!firebase.auth().currentUser) {
      // Go to login
      next({
        path: '/session/login',
        query: {
          redirect: to.fullPath
        }
      });
    } else {
      // Proceed to route
      next();
    }
  } else if (to.matched.some(record => record.meta.requiresGuest)) {
    // Check if NO logged user
    if (firebase.auth().currentUser) {
      // Go to login
      next({
        path: '/',
        query: {
          redirect: to.fullPath
        }
      });
    } else {
      // Proceed to route
      next();
    }
  } else {
    // Proceed to route
    next();
  }
});*/
/*
router.beforeEach((to, from, next) => {
	document.title = to.meta.title;
	console.log("-----entra beforeEach0 requiresAuth",to.meta.requiresAuth )
	console.log("-----entra beforeEach0 logged",store.state.auth.logged )
	console.log("-----entra beforeEach0 loaded",store.state.loaded )

	//Nprogress.start()
	if (to.matched.some(record => record.meta.requiresAuth) ) {
		// this route requires auth, check if logged in
		// if not, redirect to login page.
		if (store.getters.logged === false && store.state.loaded==false) {
			console.log("beforeEach si logged es false 1" )

			next({
				path: '/session/login',
				query: { redirect: to.fullPath }
			})
		} else {
			console.log("beforeEach  entra  / si logged es true 2" )
			next()

		
		}
	} else {
		console.log("!!!!beforeEach si no esta autentiacda 3 " )
		//router.push("/session/login");
		console.log(store.getters.logged )


		next() // make sure to always call next()!

		
	}
}); */

	//if(store.state.auth.logged!=undefined && to.meta.requiresAuth ==false ){
		//if (to.meta.requiresAuth ==false  && store.state.auth.logged == true ){ 
	/*		if (to.meta.requiresAuth ==true  && store.state.auth.logged == false ){ 
				console.log("entra sin loged ")
				if (to.meta.requiresAuth ==true  && store.state.auth.logged == true ){ 
					console.log("entra sin requiresAuth true logged true ")
					next({path: '/'});
					return;
				}
				else { 
				next({path: '/session/login'});
			}
			}
			else{ */
	
/*	if (to.meta.requiresAuth && !store.state.auth.logged && store.state.loaded) {
		console.log("entra beforeEach 1")
	  next({path: '/session/login'});
	
	} else {
	  if (to.meta.role) {
	if (store.state.loaded && (to.meta.role !== store.state.auth.role)) {
		console.log("entra beforeEach2")
		  next({path: '/'});
		  return;
		}
	  }

	  next();

	  
	}*/

//}




  
  export default router;
  