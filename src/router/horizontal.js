import HorizontalLayout from 'Container/HorizontalLayout'

// dashboard components
const Ecommerce = () => import('Views/dashboard/Ecommerce');
const WebAnalytics = () => import('Views/dashboard/WebAnalytics');
const MagazineAndNews = () => import('Views/dashboard/MagazineAndNews');
const Agency = () => import('Views/dashboard/Agency');

export default {
   path: '/horizontal',
   component: HorizontalLayout,
   redirect: '/horizontal/dashboard/ecommerce',
   children: [
      {
         component: Ecommerce,
         path: '/horizontal/dashboard/ecommerce',
         meta: {
            requiresAuth: true,
            title: 'message.ecommerce',
            breadcrumb: 'Dashboard / Ecommerce'
         }
      },
      {
         component: WebAnalytics,
         path: '/horizontal/dashboard/web-analytics',
         meta: {
            requiresAuth: true,
            title: 'message.webAnalytics',
            breadcrumb: 'Dashboard / Web Analytics'
         }
      },
      {
         component: MagazineAndNews,
         path: '/horizontal/dashboard/magazine-and-news',
         meta: {
            requiresAuth: true,
            title: 'message.magazineAndNews',
            breadcrumb: 'Dashboard / Magazine & News'
         }
      },
      {
         component: Agency,
         path: '/horizontal/dashboard/agency',
         meta: {
            requiresAuth: true,
            title: 'message.agency',
            breadcrumb: 'Dashboard / Agency'
         }
      }
   ]
}