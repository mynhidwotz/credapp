// Sidebar Routers
export const category1 = [
   {
      action: 'zmdi-view-dashboard',
      title: 'message.dashboard',
      items: [
         { title: 'message.ecommerce', path: '/default/dashboard/ecommerce', exact: true },
         { title: 'message.webAnalytics', path: '/mini/dashboard/web-analytics', exact: true },
         { title: 'message.magazineAndNews', path: '/horizontal/dashboard/magazine-and-news', exact: true },
         { title: 'message.agency', path: '/boxed/dashboard/agency', exact: true }
      ]
   }
]
