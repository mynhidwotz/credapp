export const category0 = [
	{
		action: 'zmdi-home',
		title: 'message.home',
		items: null,
		path: '/',
		exact: true
	},

]

// Sidebar Routers
export const category1 = [  //Registro de niños  -nuevo niño, listado de niños, estadica de niños
	{
		action: 'zmdi-male',
		title: 'message.dashboard',
		items: [
			// { title: 'message.ecommerce', path: '/default/dashboard/ecommerce', exact: true },
			// { title: 'message.webAnalytics', path: '/mini/dashboard/web-analytics', exact: true },
			// { title: 'message.magazineAndNews', path: '/horizontal/dashboard/magazine-and-news', exact: true },
			// { title: 'message.agency', path: '/boxed/dashboard/agency', exact: true },
			{ title: 'message.regNinos', path: '/default/dashboard/regNinos', exact: true },
			{ title: 'message.listaNinos', path: '/default/dashboard/listaNinos', exact: true },
			{ title: 'message.statNinos', path: '/default/dashboard/statNinos', exact: true },
		]
	}
]

export const category2 = [ //Visitas medicas -nueva visita  - historial de visitas -estadistica de visitas - 
	// Mediciones antromentricas - informes de niños, 
	{
		action: 'zmdi-eye',
		title: 'message.visitas',
		items: [
			{ title: 'message.visitaMedica', path: '/default/dashboard/visitaMedica', exact: true },
			{ title: 'message.histoVisitas', path: '/default/dashboard/histoVisitas', exact: true },
			{ title: 'message.statVisitas', path: '/default/dashboard/statVisitas', exact: true },
			{ title: 'message.statMediciones', path: '/default/dashboard/statMediciones', exact: true }

			
		]
	}
]

export const category3 = [ //agenda
	{
		action: 'zmdi-calendar',
		title: 'message.agenda',
		items: [
			{ title: 'message.visitaMedica', path: '/default/dashboard/visitaMedica', exact: true },
			
		]
	}
]

export const category4 = [ //Configuraciones
	{
		action: 'zmdi-wrench',
		title: 'message.configura',
		items: [
			{ title: 'message.userProfile', path: '/default/dashboard/configUsuario', exact: true },
			{ title: 'message.configCustom', path: '/default/dashboard/configCustom', exact: true }

		]
	}
]




export const category5 = [ //ayuda
	{
		action: 'zmdi-pin-help',
		title: 'message.ayuda',
		items: [
			{ title: 'message.usoAyuda', path: '/default/dashboard/usoAyuda', exact: true },
			{ title: 'message.problemaAyuda', path: '/default/dashboard/problemaAyuda', exact: true },
			
		]
	}
]


