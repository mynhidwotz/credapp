
import router from '../router'
import { store } from '../store/store';
import EventEmitter from 'eventemitter3'

class AuthService {

  constructor() {
    this.login = this.login.bind(this)
    this.logout = this.logout.bind(this)
    this.isAuthenticated = this.isAuthenticated.bind(this)
    this.authenticated = this.isAuthenticated()
  //  this.authNotifier = new EventEmitter()
  this.authNotifier = new EventEmitter()

  }

  login() {

  }

  handleAuthentication() {
  

  }

  logout() {

  }

  isAuthenticated() {
 console.log("isAuthenticated",store.getters.getUser)
 
  }
}

export default AuthService;