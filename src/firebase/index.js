/**
 * Firebase Login
 * Vuely comes with built in firebase login feature
 * You Need To Add Your Firsebase App Account Details Here
 */
import firebase from 'firebase/app';
import 'firebase/firestore';
// Initialize Firebase 

const config = {
    messagingSenderId: "598228895769",
    apiKey: "AIzaSyAi9y-0rIwKGsjVY710MRIhH7L5qE5CfoE", // Your Api key will be here
    authDomain: "credapp-6b603.firebaseapp.com", // Your auth domain
    databaseURL: "https://credapp-6b603.firebaseio.com", // data base url
    projectId: "credapp-6b603", // project id
    storageBucket: "credapp-6b603.appspot.com",// storage bucket
    messagingSenderId: "297259247852",  // messaging sender id
   
};

firebase.initializeApp(config);

const auth = firebase.auth();
const googleAuthProvider = new firebase.auth.GoogleAuthProvider();
//const facebookAuthProvider = new firebase.auth.FacebookAuthProvider();
//const githubAuthProvider = new firebase.auth.GithubAuthProvider();
//const twitterAuthProvider = new firebase.auth.TwitterAuthProvider();
const database = firebase.firestore();//database();

export  {
    auth,
    googleAuthProvider,
    // githubAuthProvider,
    //facebookAuthProvider,
    //twitterAuthProvider,
    database
};
