/**
 * Auth Module
 */
import firebase from 'firebase';
import Firebase from 'firebase';
import Nprogress from 'nprogress';

import VueNotifications from 'vue-notifications';
import router from '../../../router';
import {
    googleAuthProvider,
    
} from '../../../firebase';

const state = {
    user: null,//Firebase.auth().currentUser ,//localStorage.getItem('userId'),
    role: 'guest',
    logged: false
}

// getters
const getters = {
    logged: (state) => {
        return !!state.user;
      },
    getUser: state => {
        return state.user;
    },
    role: (state) => {
        return state.role;
      }

}

// actions
const actions = {

   

      setUser (context) {
        context.commit('setUser');
      },
    signinUserInFirebase(context, payload) {
        const { user } = payload;
        context.commit('loginUser');
        firebase.auth().signInWithEmailAndPassword(user.email, user.password)
            .then(user => {
                Nprogress.done();
                setTimeout(() => {
                    context.commit('loginUserSuccess', user);
                }, 500)
            })
            .catch(error => {
                context.commit('loginUserFailure', error);
            });
    },
    logoutUserFromFirebase(context) {
        Nprogress.start();
        firebase.auth().signOut()
            .then(() => {
                Nprogress.done();
                setTimeout(() => {
                    context.commit('logoutUser');
                }, 500)
            })
            .catch(error => {
                context.commit('loginUserFailure', error);
            })
    },

    signinUserWithGoogle(context) {
        context.commit('loginUser');
        firebase.auth().signInWithPopup(googleAuthProvider).then((result) => {
            Nprogress.done();
            setTimeout(() => {
                context.commit('loginUserSuccess', result.user);
            }, 500)
        }).catch(error => {
            context.commit('loginUserFailure', error);
        });
    },


    signupUserInFirebase(context, payload) {
        let { userDetail } = payload;
        context.commit('signUpUser');
        firebase.auth().createUserWithEmailAndPassword(userDetail.email, userDetail.password)
            .then(() => {
                Nprogress.done();
                setTimeout(() => {
                    context.commit('signUpUserSuccess', userDetail);
                }, 500)
            })
            .catch(error => {
                context.commit('signUpUserFailure', error);
            })
    },

}

// mutations
const mutations = {
    setUser: (state, user) => {
    /*    if ( user ) {
          state.user = {
            uid: user.uid,
            email: user.email
          };
          state.logged = true;
        } else {
          state.user = null;
          state.logged = false;
        }*/
        state.user = Firebase.auth().currentUser; 
      },

      setRole: (state, role) => {
        state.role = role;
      },
    loginUser(state) {
        Nprogress.start();
    },
    loginUserSuccess(state, user) {
        state.user = user;// localStorage.setItem('userId', user);
    
        router.push("/dashboard");
        VueNotifications.success({ message: 'User Logged In Success!' });
    },
    loginUserFailure(state, error) {
        Nprogress.done();
        VueNotifications.error({ message: error });
    },
    logoutUser(state) {
        state.user = null
        //localStorage.removeItem('userId');
        router.push("/login");
    },
    signUpUser(state) {
        Nprogress.start();
    },
    signUpUserSuccess(state, user) {
      //  state.user = localStorage.setItem('userId', user);
        router.push("/default/dashboard/ecommerce");
        VueNotifications.success({ message: 'Account Created!' });
    },
    signUpUserFailure(state, error) {
        Nprogress.done();
        VueNotifications.error({ message: error.message });
    },

}

export default {
    state,
    getters,
    actions,
    mutations
}
