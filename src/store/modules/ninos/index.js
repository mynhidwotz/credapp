/**
 * Ninos Module
 */

import firebase from 'firebase';
import Nprogress from 'nprogress';
import moment from "moment";
import VueNotifications from 'vue-notifications';
import router from '../../../router';
import {
    // facebookAuthProvider,

    database,

    // twitterAuthProvider,
    // githubAuthProvider
} from '../../../firebase';

const settings = {
    timestampsInSnapshots: true
};
database.settings(settings);

const state = {
    nino: {

        name: "",
        tel1: "",
        sexo: "",
        fechaNac: "",
        email: ""

    }
}


// getters
const getters = {


}

// actions
const actions = {



    ninoIngresadoInFirebase(context, payload) {
        let {
            ninoDetail
        } = payload;
        context.commit('creandoNino');
        console.log(ninoDetail.name)

        var user = firebase.auth().currentUser;
        ninoDetail.uid = user.uid

        console.log("ingresando en el store ninos", user.uid)
        //cambiar id 
        database.collection("ninos").doc().set(
                ninoDetail

            ).then(function () {
                console.log("se registro el niño!");

                  //  database.collection("dashboard").doc(user.uid).set({totalNinos:1})

                Nprogress.done();
                setTimeout(() => {
                    context.commit('NinoCreadoSuccess', ninoDetail);
                }, 500)
            })
            .catch(function (error) {
                context.commit('NinoCreadoFailure', error);
                console.error("error al registrar niño: ", error);
            });



        /*   database.collection('ninos').doc(u).onSnapshot(cartSnap => {

          //  commit('setCart', {cart, cartId: user.uid});
          database.collection('ninos').doc(u).set({
            name:ninoDetail.name,
            tel1:ninoDetail.tel1,
            sexo:ninoDetail.sexo,
            fechaNac:ninoDetail.date,
            email: ninoDetail.email
           
        });
          }, (error) => {
            console.log('listener cart off...');
          }); 
        */


    }



}

// mutations
const mutations = {
    creandoNino(state) {
        Nprogress.start();
    },
    NinoCreadoSuccess(state, user) {
           router.push("/");
        VueNotifications.success({
            message: '¡Niño registrado con exito!'
        });
    },
    NinoCreadoFailure(state, error) {
        Nprogress.done();
        VueNotifications.error({
            message: error.message
        });
    },
}

export default {
    state,
    getters,
    actions,
    mutations
}