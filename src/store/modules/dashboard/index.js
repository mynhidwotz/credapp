/**
 * Dashboard Module
 */

import firebase from 'firebase';

import router from '../../../router';
import {
    // facebookAuthProvider,

    database,

    // twitterAuthProvider,
    // githubAuthProvider
} from '../../../firebase';
const settings = {
    timestampsInSnapshots: true
};
database.settings(settings);

const state = {
    dashId: null,
    totalNinos: 0,

}

// getters
const getters = {

    totalNinos: state => {
        return state.totalNinos;
     },
}

// actions
const actions = {
    createDashboardIfNotExists({commit, state}, user) {
        database.collection('dashboard').doc(user.uid).onSnapshot(dashSnap => {
            let dash;
            if (!dashSnap.exists) {
                 console.log("no existe coleection dashboard")
            dash = {
             totalNinos: 0
              };
              database.collection('dashboard').doc(user.uid).set(dash);
            }
            else {
                dash = dashSnap.data();
              }
              commit('setDash', {dash, dashId: user.uid});
        }, (error) => {
        console.log('listener dash off...');
      });
    },

    updateDash (context, data) {
        console.log ('state.dashId',state.dashId);


        let dash = database.collection('dashboard').doc(state.dashId);
        console.log ('dash',dash);
    //    const nino = data.type;
    
        dash.get().then((doc) => {
            let dashData = doc.data();
          if (data.type === 'increment') {
                dashData.totalNinos += 1;
            }else {
                if (data.type === 'equal') {
                 
                }


            } 


            dash.update(dashData).then(() => {
                context.commit('updateDash', dashData);
              })
        })

      } //updateDash

}


// mutations
const mutations = {
    setDash (state, data) {
        //state.cart = data.cart.products;
        state.totalNinos = data.dash.totalNinos;
        state.dashId = data.dashId;
      },
      updateDash (state, data) {
        //state.cart = data.products;
        state.totalNinos = data.totalNinos;
      }
}


export default {
    state,
    getters,
    actions,
    mutations
}