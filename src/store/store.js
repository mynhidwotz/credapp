import Vue from 'vue'
import Vuex from 'vuex'

// modules
import auth from './modules/auth';
import ninos from './modules/ninos';
//import chat from './modules/chat';
import settings from './modules/settings';
import dashboard from './modules/dashboard';
//import ecommerce from './modules/ecommerce';
//import mail from './modules/mail';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
      
        loaded: false,
   
      },
      actions:{
        logoutUserFrom:(state, loaded) => {
          state.loaded = false;
        }
       
      },
      mutations: {
        setLoaded: (state, loaded) => {
          state.loaded = loaded;
        }
      },
    modules: {
        auth,
        ninos,
     //   chat,
        settings,
        dashboard
   //     ecommerce,
   //     mail
    }
})
